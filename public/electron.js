const electron = require("electron");
const { app, BrowserWindow, Menu } = require("electron");

const path = require("path");
const url = require("url");
const isDev = require("electron-is-dev");

const globalShortcut = electron.globalShortcut;

Menu.setApplicationMenu(null);

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({ width: 1280, height: 720, toolbar: false });
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );

  if (isDev) {
    mainWindow.toggleDevTools();
  }

  mainWindow.on("closed", () => (mainWindow = null));

  globalShortcut.register("f5", () => {
    mainWindow.reload();
  });
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
