import React from "react";
import ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";

import { syncHistoryWithStore } from "mobx-react-router";

import { routingStore } from "rootStore";

import App from "./App";

import "antd/dist/antd.css";
import "./index.css";

const browserHistory = createBrowserHistory();

const history = syncHistoryWithStore(browserHistory, routingStore);

ReactDOM.render(
  <Router history={history}>
    <App />
  </Router>,
  document.getElementById("root")
);
