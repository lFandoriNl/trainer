import React from "react";

import { Typography, Radio } from "antd";

import { observer } from "mobx-react";

import styled from "styled-components/macro";

const Wrapper = styled.div`
  .ant-radio-wrapper {
    margin: 10px 0;
    display: flex;
    align-items: center;
  }

  .answer {
    font-size: 18px;

    .ant-input {
      margin-left: 5px;
      width: 40px;
    }
  }
`;

const Answers = observer(({ question }) => {
  const onChange = event => {
    question.userAnswer = event.target.value;
  };

  return (
    <Wrapper>
      <Typography.Title level={3}>
        <span dangerouslySetInnerHTML={{ __html: question.text }} />
      </Typography.Title>

      <Radio.Group value={question.userAnswer} onChange={onChange}>
        {question.answersRadio &&
          question.answersRadio.map(answer => (
            <Radio key={answer.value} value={answer.value}>
              <span
                className="answer"
                dangerouslySetInnerHTML={{ __html: answer.text }}
              />
            </Radio>
          ))}

        {question.answersInput && (
          <span className="answer">{question.answersInput.text}</span>
        )}
      </Radio.Group>
    </Wrapper>
  );
});

export default Answers;
