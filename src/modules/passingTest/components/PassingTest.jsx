import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

import { Button } from "antd";

import Answers from "./Answers";
import TestResult from "./TestResult";

import { observer } from "mobx-react";
import { useTestStore } from "shared/hooks/tests/useTestStore";

import styled from "styled-components/macro";

const Wrapper = styled.div`
  margin-top: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Question = styled.div`
  min-width: 300px;
`;

const ButtonWrapper = styled.div`
  margin-top: 30px;
  text-align: right;
`;

const PassingTest = observer(() => {
  const { id } = useParams();
  const testStore = useTestStore();

  const test = testStore.getTestById(id);

  useEffect(() => {
    test.startPassing();
  }, []);

  const onSubmit = event => {
    event.preventDefault();

    const testQuestion = test.questions[test.currentQuestion];

    if (testQuestion.answersInput) {
      const userAnswer = {};
      Object.keys(testQuestion.correctAnswer).forEach(key => {
        userAnswer[key] = event.target.elements[key].value.replace(",", ".");
      });

      testQuestion.userAnswer = userAnswer;
    }

    test.reply();
  };

  return (
    <Wrapper>
      {test.result.finished ? (
        <TestResult title={test.title} result={test.result} />
      ) : (
        <Question>
          <form onSubmit={onSubmit}>
            <div>
              {test.currentQuestion + 1} / {test.questions.length}
            </div>

            <br />

            <Answers question={test.questions[test.currentQuestion]} />

            <ButtonWrapper>
              <Button htmlType="submit" type="primary" ghost>
                Відповісти
              </Button>
            </ButtonWrapper>
          </form>
        </Question>
      )}
    </Wrapper>
  );
});

export default PassingTest;
