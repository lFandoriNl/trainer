import React from "react";
import { Link } from "react-router-dom";

import { Result, Button } from "antd";

import { observer } from "mobx-react";

const TestResult = observer(({ title, result }) => {
  return (
    <Result
      status={result.passed ? "success" : "error"}
      title={
        result.passed
          ? `Ви успішно закінчили тест на тему "${title}"`
          : `Ви провалили тест на тему "${title}"`
      }
      subTitle={`Ви дали ${result.correctAnswersCount} правильних відповідей з ${result.questionsCount}!`}
      extra={[
        // <Button key="1" type="primary">
        //   <Link to="/">На главную</Link>
        // </Button>,
        <Button key="2" type="primary">
          <Link to="/tests">Повернутись до тестів</Link>
        </Button>
      ]}
    />
  );
});

export default TestResult;
