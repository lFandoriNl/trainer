import { observable, action, decorate } from "mobx";

class DrawerStore {
  isOpen = false;
  activeKey = null;
  tabs = null;
  options = {};

  open({ tabs, activeKey = null, options = {} }) {
    this.isOpen = true;
    this.tabs = tabs;

    if (activeKey) {
      this.activeKey = tabs[0] ? tabs[0].key : activeKey;
    } else {
      this.activeKey = activeKey;
    }

    this.options = options;
  }

  close() {
    this.isOpen = false;
  }
}

decorate(DrawerStore, {
  isOpen: observable,
  tabs: observable,
  activeKey: observable,
  open: action,
  close: action
});

export { DrawerStore };
