import React from "react";
import { Link } from "react-router-dom";

import { Card, Button, Icon } from "antd";

import { observer } from "mobx-react";

const TestCard = observer(({ test }) => {
  return (
    <Card
      title={test.title}
      extra={
        <div>
          {test.completed ? (
            <>
              Пройдено <Icon type="check" style={{ color: "green" }} />
            </>
          ) : (
            <>
              Не пройдено <Icon type="close" style={{ color: "red" }} />
            </>
          )}
        </div>
      }
      actions={
        test.completed
          ? []
          : [
              <Button type="primary" ghost>
                <Link to={`/tests/${test.id}`}>Пройти</Link>
              </Button>
            ]
      }
    >
      {test.description}
      <br />
      Кількість спроб: {test.numberAttempts}
    </Card>
  );
});

export default TestCard;
