import React from "react";

import { Form, Input, Checkbox } from "antd";

import { observer } from "mobx-react";

export const FormItemTitle = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(1);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("title", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemDescription = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(2);

    return (
      <Form.Item label="Описание">
        {getFieldDecorator("description", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Описание"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField1 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(3);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field1", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField2 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(4);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field2", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField3 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(5);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field3", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField4 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(6);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field4", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField5 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(7);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field5", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField6 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(8);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field6", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField7 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(9);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field7", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField8 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(10);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field8", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField9 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(11);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field9", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemField10 = observer(
  ({ fieldName, handler, handlers, validateFields, getFieldDecorator }) => {
    console.log(12);

    return (
      <Form.Item label="Название">
        {getFieldDecorator("field10", {
          rules: [{ required: true, message: "Пожалуйста заполните поле!" }]
        })(
          <Input
            placeholder="Название"
            onChange={event =>
              handlers[handler](validateFields, fieldName, event)
            }
          />
        )}
      </Form.Item>
    );
  }
);

export const FormItemCompleted = observer(({ getFieldDecorator }) => {
  console.log(13);

  return (
    <Form.Item>
      {getFieldDecorator("completed", {
        valuePropName: "checked",
        initialValue: false
      })(<Checkbox>Выполнено</Checkbox>)}
    </Form.Item>
  );
});
