import React from "react";

import { Form, Button } from "antd";

import CustomSteps from "shared/components/form/CustomSteps";

import {
  FormItemTitle,
  FormItemDescription,
  FormItemField1,
  FormItemField2,
  FormItemField3,
  FormItemField4,
  FormItemField5,
  FormItemField6,
  FormItemField7,
  FormItemField8,
  FormItemField9,
  FormItemField10,
  FormItemCompleted
} from "./fields";

import { observable, action } from "mobx";
import { observer } from "mobx-react";
import { useStore } from "shared/hooks/useStore";

const handlers = observable(
  {
    title: "wait",
    description: "wait",
    completed: "finish",

    field1: "wait",
    field2: "wait",
    field3: "wait",
    field4: "wait",
    field5: "wait",
    field6: "wait",
    field7: "wait",
    field8: "wait",
    field9: "wait",
    field10: "wait",

    handleInput(validateFields, fieldName, event) {
      event.preventDefault();

      setTimeout(() => {
        validateFields([fieldName], error => {
          this[fieldName] = error ? "error" : "finish";
        });
      }, 0);
    }
  },
  {
    handleInput: action
  }
);

const CreateTestForm = observer(
  ({
    isEdit,
    test,
    form: { getFieldValue, validateFields, getFieldDecorator }
  }) => {
    const { testsStore } = useStore(store => store.tests);
    const { drawerStore } = useStore(store => store.navigation);

    const handleSubmit = event => {
      event.preventDefault();
      validateFields((errors, values) => {
        if (errors) {
          return Object.keys(errors).forEach(error => {
            handlers[error] = "error";
          });
        }

        if (isEdit) {
          test.edit(values);
        } else {
          testsStore.addTest(values);
        }

        drawerStore.close();
      });
    };

    const fields = [
      {
        key: "title",
        title: "Название",
        status: handlers.title,
        component: (
          <FormItemTitle
            fieldName="title"
            value={getFieldValue("title")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "description",
        title: "Описание",
        status: handlers.description,
        component: (
          <FormItemDescription
            fieldName="description"
            value={getFieldValue("description")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field1",
        title: "field1",
        status: handlers.field1,
        component: (
          <FormItemField1
            fieldName="field1"
            value={getFieldValue("field1")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field2",
        title: "field2",
        status: handlers.field2,
        component: (
          <FormItemField2
            fieldName="field2"
            value={getFieldValue("field2")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field3",
        title: "field3",
        status: handlers.field3,
        component: (
          <FormItemField3
            fieldName="field3"
            value={getFieldValue("field3")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field4",
        title: "field4",
        status: handlers.field4,
        component: (
          <FormItemField4
            fieldName="field4"
            value={getFieldValue("field4")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field5",
        title: "field5",
        status: handlers.field5,
        component: (
          <FormItemField5
            fieldName="field5"
            value={getFieldValue("field5")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field6",
        title: "field6",
        status: handlers.field6,
        component: (
          <FormItemField6
            fieldName="field6"
            value={getFieldValue("field6")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field7",
        title: "field7",
        status: handlers.field7,
        component: (
          <FormItemField7
            fieldName="field7"
            value={getFieldValue("field7")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field8",
        title: "field8",
        status: handlers.field8,
        component: (
          <FormItemField8
            fieldName="field8"
            value={getFieldValue("field8")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field9",
        title: "field9",
        status: handlers.field9,
        component: (
          <FormItemField9
            fieldName="field9"
            value={getFieldValue("field9")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "field10",
        title: "field10",
        status: handlers.field10,
        component: (
          <FormItemField10
            fieldName="field10"
            value={getFieldValue("field10")}
            handler="handleInput"
            handlers={handlers}
            validateFields={validateFields}
            getFieldDecorator={getFieldDecorator}
          />
        )
      },
      {
        key: "completed",
        title: "Выполнено",
        status: handlers.completed,
        component: (
          <FormItemCompleted
            value={getFieldValue("completed")}
            getFieldDecorator={getFieldDecorator}
          />
        )
      }
    ];

    return (
      <Form onSubmit={handleSubmit}>
        <CustomSteps fields={fields} />

        <Button type="primary" htmlType="submit">
          {isEdit ? "Редактировать" : "Создать"}
        </Button>
      </Form>
    );
  }
);

const WrappedCreateTestForm = Form.create({
  name: "create_test",
  mapPropsToFields({ test }) {
    if (test) {
      return {
        title: Form.createFormField({
          ...test.title,
          value: test.title
        }),
        description: Form.createFormField({
          ...test.description,
          value: test.description
        }),
        completed: Form.createFormField({
          ...test.completed,
          value: test.completed
        }),
        field1: Form.createFormField({
          ...test.field1,
          value: test.field1
        }),
        field12: Form.createFormField({
          ...test.field2,
          value: test.field2
        }),
        field3: Form.createFormField({
          ...test.field3,
          value: test.field3
        }),
        field4: Form.createFormField({
          ...test.field4,
          value: test.field5
        }),
        field5: Form.createFormField({
          ...test.field5,
          value: test.field5
        }),
        field6: Form.createFormField({
          ...test.field6,
          value: test.field6
        }),
        field7: Form.createFormField({
          ...test.field7,
          value: test.field7
        }),
        field8: Form.createFormField({
          ...test.field8,
          value: test.field8
        }),
        field9: Form.createFormField({
          ...test.field9,
          value: test.field9
        }),
        field10: Form.createFormField({
          ...test.field10,
          value: test.field10
        })
      };
    }

    return {};
  }
})(CreateTestForm);

const CreateTest = observer(() => {
  const { drawerStore } = useStore(store => store.navigation);

  return (
    <WrappedCreateTestForm
      isEdit={drawerStore.options.isEdit}
      test={drawerStore.options.test}
    />
  );
});

export default CreateTest;
