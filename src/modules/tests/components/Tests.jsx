import React from "react";

import TestCard from "./TestCard";

import { observer } from "mobx-react";
import { useTestStore } from "shared/hooks/tests/useTestStore";

import styled from "styled-components/macro";

const Wrapper = styled.div``;

const TestList = styled.div`
  display: flex;
  flex-wrap: wrap;

  .ant-card {
    margin: 0 15px 15px 0;
    width: 300px;
  }
`;

const Tests = observer(() => {
  const testStore = useTestStore();

  return (
    <Wrapper>
      <TestList>
        {testStore.tests.map(test => <TestCard key={test.id} test={test} />)}
      </TestList>
    </Wrapper>
  );
});

export default Tests;
