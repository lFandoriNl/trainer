import { observable, computed, action, decorate, toJS } from "mobx";

import { QuestionModel } from "./question";

const initialResult = {
  finished: false,
  passed: false,
  questionsCount: 0,
  correctAnswersCount: 0
};

class TestModel {
  id;
  title;
  description;
  completed;
  numberAttempts;
  questions;

  currentQuestion = 0;

  result = initialResult;

  constructor(
    testStore,
    {
      id,
      title = "",
      description = "",
      completed = false,
      numberAttempts = 0,
      questions = []
    }
  ) {
    this.testStore = testStore;

    this.id = id || Date.now();
    this.title = title;
    this.description = description;
    this.completed = completed;
    this.numberAttempts = numberAttempts;
    this.questions = questions.map(
      question => new QuestionModel(this, question)
    );
  }

  startPassing() {
    this.result = initialResult;
  }

  reply() {
    if (this.result.finished === false) {
      this.currentQuestion += 1;

      if (this.currentQuestion === this.questions.length) {
        this.checkResult();
      }
    }
  }

  checkResult() {
    let correctAnswersCount = 0;

    this.questions.forEach(question => {
      if (question.answersRadio) {
        if (question.userAnswer === question.correctAnswer) {
          correctAnswersCount += 1;
        }
      } else {
        const isAllInputsCorrect = Object.keys(question.userAnswer).every(
          key => question.userAnswer[key] === question.correctAnswer[key]
        );

        if (isAllInputsCorrect) {
          correctAnswersCount += 1;
        }
      }
    });

    this.result.finished = true;
    this.result.questionsCount = this.questions.length;
    this.result.correctAnswersCount = correctAnswersCount;

    if (
      Math.round(
        (this.result.correctAnswersCount / this.result.questionsCount) * 100
      ) >= 67
    ) {
      this.result.passed = true;
      this.completed = true;
    } else {
      this.result.passed = false;
    }

    this.numberAttempts++;
    this.questions.forEach(question => question.clearUserAnswer());
    this.resetCurrentQuestion();
  }

  resetCurrentQuestion() {
    this.currentQuestion = 0;
  }

  get toJS() {
    return toJS(this);
  }
}

decorate(TestModel, {
  id: observable,
  title: observable,
  description: observable,
  completed: observable,
  questions: observable,

  currentQuestion: observable,
  result: observable,

  startPassing: action,
  reply: action,
  checkResult: action,
  resetCurrentQuestion: action,
  toJS: computed
});

export { TestModel };
