import { observable, computed, action, decorate, toJS } from "mobx";

class QuestionModel {
  id;
  text;
  answersRadio;
  answersInput;
  correctAnswer;
  userAnswer;

  constructor(
    testModel,
    {
      id,
      text = "",
      answersRadio,
      answersInput,
      correctAnswer = "",
      userAnswer = ""
    }
  ) {
    this.testModel = testModel;

    this.id = id || Date.now();
    this.text = text;
    this.answersRadio = answersRadio;
    this.answersInput = answersInput;
    this.correctAnswer = correctAnswer;
    this.userAnswer = userAnswer;
  }

  clearUserAnswer() {
    this.userAnswer = undefined;
  }

  get toJS() {
    return toJS(this);
  }
}

decorate(QuestionModel, {
  id: observable,
  text: observable,
  correctAnswer: observable,
  userAnswer: observable,
  clearUserAnswer: action,
  toJS: computed
});

export { QuestionModel };
