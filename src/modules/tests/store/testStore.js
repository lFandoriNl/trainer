import React from "react";
import { Input } from "antd";

import { observable, computed, decorate, toJS } from "mobx";

import { TestModel } from "./models/test";

const initialTests = [
  {
    id: "1",
    title: "Тест №1",
    description: "",
    numberAttempts: 0,
    questions: [
      {
        id: "1",
        text: "1. Середньою швидкістю зміни функції є: ",
        answersRadio: [
          {
            value: "1",
            text: "Відношення приросту функції до приросту аргументу."
          },
          {
            value: "2",
            text: "Відношення приросту аргументу до приросту функції."
          }
        ],
        correctAnswer: "1"
      },
      {
        id: "2",
        text: "2. Швидкістю зміни функції є: ",
        answersRadio: [
          {
            value: "1",
            text:
              "Границя відношення приросту функції до приросту аргументу<br /> за умови, що приріст аргументу прямує до безкінечності."
          },
          {
            value: "2",
            text:
              "Границя відношення приросту функції до приросту аргументу<br /> за умови, що приріст аргументу не прямує до безкінечності."
          },
          {
            value: "3",
            text:
              "Границя відношення приросту функції до приросту аргументу<br /> за умови, що приріст аргументу прямує до нуля"
          },
          {
            value: "4",
            text:
              "Границя відношення приросту функції до приросту аргументу<br /> за умови, що приріст аргументу не прямує до нуля"
          }
        ],
        correctAnswer: "3"
      },
      {
        id: "3",
        text: "3. Виберіть правильну відповідь 3x<sup>2</sup> + 4xy - 7cos y",
        answersRadio: [
          {
            value: "1",
            text: "Z'<sub>x</sub> = 6x + 4y"
          },
          {
            value: "2",
            text: "Z'<sub>x</sub> = 6x + 4x"
          },
          {
            value: "3",
            text: "Z'<sub>x</sub> = 4y + 7sin y"
          },
          {
            value: "4",
            text: "Z'<sub>x</sub> = 4x + 7sin y"
          }
        ],
        correctAnswer: "1"
      },
      {
        id: "4",
        text: "4. Виберіть правильну відповідь Z'<sub>y</sub> = 4y + 7sin y",
        answersRadio: [
          {
            value: "1",
            text: "Z'<sub>y</sub> = 6x + 4y"
          },
          {
            value: "2",
            text: "Z'<sub>y</sub> = 4x + 7sin y"
          },
          {
            value: "3",
            text: "Z'<sub>y</sub> = 4y + 7sin y"
          },
          {
            value: "4",
            text: "Z'<sub>y</sub> = 6x + 4sx"
          }
        ],
        correctAnswer: "2"
      },
      {
        id: "5",
        text: "5. Знайти частичну похідну Z'<sub>x</sub>",
        answersInput: {
          text: (
            <>
              Z = 2x<sup>5</sup> y<sup>8</sup>
              <br />
              <br />
              Z'<sub>x</sub> = <Input name="1-5-1" size="small" />x
              <sup style={{ top: "-0.7em" }}>
                <Input name="1-5-2" size="small" />
              </sup>{" "}
              y
              <sup style={{ top: "-0.7em" }}>
                <Input name="1-5-3" size="small" />
              </sup>
            </>
          )
        },
        correctAnswer: { "1-5-1": "10", "1-5-2": "4", "1-5-3": "8" }
      },
      {
        id: "6",
        text: "6. Знайти частичну похідну Z'<sub>y</sub>",
        answersInput: {
          text: (
            <>
              Z = 4x<sup>2</sup> + 2xy + 3y<sup>2</sup>
              <br />
              <br />
              Z'<sub>y</sub> = <Input name="1-6-1" size="small" />x +{" "}
              <Input name="1-6-2" size="small" />y
            </>
          )
        },
        correctAnswer: { "1-6-1": "2", "1-6-2": "6" }
      }
    ]
  },
  {
    id: "2",
    title: "Тест №2",
    description: "",
    numberAttempts: 0,
    questions: [
      {
        id: "1",
        text: "1. Середньою швидкістю зміни функції є: ",
        answersRadio: [
          {
            value: "1",
            text: "Відношення приросту функції до приросту аргументу."
          },
          {
            value: "2",
            text: "Відношення приросту аргументу до приросту функції."
          }
        ],
        correctAnswer: "1"
      },
      {
        id: "2",
        text: "2. Швидкістю зміни функції є: ",
        answersRadio: [
          {
            value: "1",
            text:
              "Границя відношення приросту функції до приросту аргументу<br /> за умови, що приріст аргументу прямує до безкінечності."
          },
          {
            value: "2",
            text:
              "Границя відношення приросту функції до приросту аргументу<br /> за умови, що приріст аргументу не прямує до безкінечності."
          },
          {
            value: "3",
            text:
              "Границя відношення приросту функції до приросту аргументу<br /> за умови, що приріст аргументу прямує до нуля"
          },
          {
            value: "4",
            text:
              "Границя відношення приросту функції до приросту аргументу<br /> за умови, що приріст аргументу не прямує до нуля"
          }
        ],
        correctAnswer: "3"
      },
      {
        id: "3",
        text: "3. Виберіть правильну відповідь 3x<sup>2</sup> + 4xy - 7cos y",
        answersRadio: [
          {
            value: "1",
            text: "Z'<sub>x</sub> = 6x + 4y"
          },
          {
            value: "2",
            text: "Z'<sub>x</sub> = 6x + 4x"
          },
          {
            value: "3",
            text: "Z'<sub>x</sub> = 4y + 7sin y"
          },
          {
            value: "4",
            text: "Z'<sub>x</sub> = 4x + 7sin y"
          }
        ],
        correctAnswer: "1"
      },
      {
        id: "4",
        text: "4. Виберіть правильну відповідь Z'<sub>y</sub> = 4y + 7sin y",
        answersRadio: [
          {
            value: "1",
            text: "Z'<sub>y</sub> = 6x + 4y"
          },
          {
            value: "2",
            text: "Z'<sub>y</sub> = 4x + 7sin y"
          },
          {
            value: "3",
            text: "Z'<sub>y</sub> = 4y + 7sin y"
          },
          {
            value: "4",
            text: "Z'<sub>y</sub> = 6x + 4sx"
          }
        ],
        correctAnswer: "2"
      },
      {
        id: "5",
        text: "5. Знайти частичну похідну Z'<sub>x</sub>",
        answersInput: {
          text: (
            <>
              Z = 2x<sup>5</sup> y<sup>8</sup>
              <br />
              <br />
              Z'<sub>x</sub> = <Input name="1-5-1" size="small" />x
              <sup style={{ top: "-0.7em" }}>
                <Input name="1-5-2" size="small" />
              </sup>{" "}
              y
              <sup style={{ top: "-0.7em" }}>
                <Input name="1-5-3" size="small" />
              </sup>
            </>
          )
        },
        correctAnswer: { "1-5-1": "10", "1-5-2": "4", "1-5-3": "8" }
      },
      {
        id: "6",
        text: "6. Знайти частичну похідну Z'<sub>y</sub>",
        answersInput: {
          text: (
            <>
              Z = 4x<sup>2</sup> + 2xy + 3y<sup>2</sup>
              <br />
              <br />
              Z'<sub>y</sub> = <Input name="1-6-1" size="small" />x +{" "}
              <Input name="1-6-2" size="small" />y
            </>
          )
        },
        correctAnswer: { "1-6-1": "2", "1-6-2": "6" }
      }
    ]
  }
];

class TestStore {
  tests = [];

  constructor(rootStore) {
    this.rootStore = rootStore;

    this.tests = initialTests.map(test => new TestModel(this, test));
  }

  getTestById(id) {
    return this.tests.find(test => test.id === id);
  }

  get toJS() {
    return toJS(this);
  }
}

decorate(TestStore, { tests: observable, toJS: computed });

export { TestStore };
