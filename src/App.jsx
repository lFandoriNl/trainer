import React from "react";
import { Switch, Route, Link } from "react-router-dom";

import { Layout, Menu } from "antd";

import Home from "modules/home";
import Tests from "modules/tests";
import PassingTest from "modules/passingTest";

import { observer } from "mobx-react";
import { useStore } from "shared/hooks/useStore";

import styled from "styled-components/macro";

const MainContent = styled(Layout.Content)`
  height: 100vh;
`;

const Header = styled(Layout.Header)`
  position: sticky;
  top: 0;
  width: 100%;
  z-index: 1;
`;

const Content = styled(Layout.Content)`
  height: calc(100vh - 64px);
  padding: 20px;
  background-color: #f9f9f9;
  border-radius: 5px;
`;

const About = () => {
  return <div>©{new Date().getFullYear()} Створено Койло Максимом</div>;
};

const App = observer(() => {
  const { routing } = useStore();

  if (routing.location.pathname === "/") {
    routing.push("/tests");
  }

  return (
    <div className="app">
      <MainContent>
        <Header>
          <Menu
            theme="dark"
            mode="horizontal"
            selectedKeys={[routing.location.pathname]}
            style={{ lineHeight: "64px" }}
          >
            {/* <Menu.Item key="/">
              <Link to="/">Главная</Link>
            </Menu.Item> */}

            <Menu.Item key="/tests">
              <Link to="/tests">Тести</Link>
            </Menu.Item>

            <Menu.Item key="/about">
              <Link to="/about">Про програму</Link>
            </Menu.Item>
          </Menu>
        </Header>

        <Content>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/tests" component={Tests} />
            <Route path="/tests/:id" component={PassingTest} />
            <Route path="/about" component={About} />
          </Switch>
        </Content>
      </MainContent>

      {/* <Modals /> */}
    </div>
  );
});

export default App;
