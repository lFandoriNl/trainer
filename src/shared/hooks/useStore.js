import React from "react";

import { storeContext } from "contexts";

export const useStore = () => React.useContext(storeContext);
