import React from "react";

import { storeContext } from "contexts";

export const useTestStore = () => React.useContext(storeContext).testStore;
