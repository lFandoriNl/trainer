/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { Drawer, Tabs } from "antd";

import { observer } from "mobx-react";
import { useStore } from "shared/hooks/useStore";

import "./style.css";

const { TabPane } = Tabs;

const DrawerBottom = observer(() => {
  const { drawerStore } = useStore(store => store.navigation);

  const { isOpen, tabs, activeKey } = drawerStore;

  let activeKeyAttribute = {};

  if (activeKey) {
    activeKeyAttribute = {
      activeKey
    };
  }

  return (
    <Drawer
      placement="right"
      visible={isOpen}
      className="drawer"
      onClose={() => drawerStore.close()}
    >
      <Tabs {...activeKeyAttribute}>
        {tabs &&
          tabs.map(tab => (
            <TabPane key={tab.key} tab={tab.title}>
              {tab.component}
            </TabPane>
          ))}
      </Tabs>
    </Drawer>
  );
});

export default DrawerBottom;
