import React from "react";

import { Steps } from "antd";

import { ReactComponent as OkIcon } from "shared/assets/img/form/StepOK.svg";
import { ReactComponent as ErrorIcon } from "shared/assets/img/form/StepError.svg";
import { ReactComponent as ProcessIcon } from "shared/assets/img/form/StepProcess.svg";

import styled from "styled-components/macro";

const StyledSteps = styled(Steps)`
  &&& {
    .ant-steps-item-finish
      > .ant-steps-item-container
      > .ant-steps-item-tail::after {
      background-color: #4caf50;
    }
    .ant-steps-item-title {
      font-size: 12px;
      color: #535c6f;
      font-weight: 600;
    }
  }
`;

const CustomSteps = ({ fields }) => {
  return (
    <StyledSteps direction="vertical">
      {fields.map(field => {
        let icon;
        switch (field.status) {
          case "finish":
            icon = <OkIcon />;
            break;
          case "error":
            icon = <ErrorIcon />;
            break;
          case "process":
            icon = <ProcessIcon />;
            break;
          default:
            icon = null;
        }

        return (
          <Steps.Step
            key={field.key}
            title={field.title}
            status={field.status}
            description={field.component}
            icon={icon}
          />
        );
      })}
    </StyledSteps>
  );
};

export default CustomSteps;
