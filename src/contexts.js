import React from "react";

import { RootStore } from "rootStore";

export const storeContext = React.createContext(new RootStore());
