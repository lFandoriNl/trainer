import { RouterStore } from "mobx-react-router";

import { TestStore } from "modules/tests/store";

const routingStore = new RouterStore();

class RootStore {
  constructor() {
    this.routing = routingStore;
    this.testStore = new TestStore(this);
  }
}

export { RootStore, routingStore };
